import CryptoJS from 'crypto-js';

/** 비밀번호 암호화 (SHA256) */
const transHash = (pw: string) => {
  const hash: string = CryptoJS.SHA256(pw).toString();

  return hash;
};

/** 암호화된 비밀번호와 입력한 비밀번호 비교 */
const comparePwWithHash = (pw: string, hashPw: string) => {
  const hash: string = CryptoJS.SHA256(pw).toString();

  return hash === hashPw;
};

export default {
  transHash,
  comparePwWithHash
};