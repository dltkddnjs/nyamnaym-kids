/* eslint-disable no-useless-escape */

export const checkId = (id: string) => {
  const regexId = /^[A-Za-z0-9]([-_.]?[A-Za-z0-9])*@[A-Za-z0-9]([-_.]?[A-Za-z0-9])*\.[A-Za-z]{2,3}$/;

  if (!regexId.test(id)) return false;
  
  return true;
};

export const checkPassword = (pw: string) => {
  const regexPw = /^.*(?=^.{8,32}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/; /* ! @ # $ % ^ & * */

  if (!regexPw.test(pw)) return false;
    
  return true;
};

export const checkNumberRegex = /[\\`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]/a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣 ]/gim;