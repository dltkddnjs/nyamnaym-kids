import Link from 'next/link';
import Image from 'next/image';
import InvitationCardImage from '../../public/assets/images/invitation-card-image.png';
import './index.scss';

const InvitationCard = () => {
  return (
    <div className="invitation-card-container">
      <p>박누비 선생님, 초대장이 도착했어요!</p>
      <figure>
        <Image src={InvitationCardImage} alt="image" />
        <figcaption>
          <p>누비어린이집</p>
          <p>달님반</p>
        </figcaption>
      </figure>
      <Link href="">가입하러 가기</Link>
    </div>
  );
};

export default InvitationCard;
