import './index.scss';

interface INextButtonProps {
  disabled: boolean;
  stepCallback:() => void;
}

const NextButton = ({ disabled, stepCallback }: INextButtonProps) => {
  return (
    <div className="next-button-wrapper" onClick={stepCallback}>
      <button disabled={disabled} type="submit">다음</button>
    </div>
  );
};

export default NextButton;
