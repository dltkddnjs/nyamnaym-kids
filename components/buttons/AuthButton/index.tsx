import './index.scss';

interface IAuthButtonProps {
  buttonTitle: string;
  buttonKey: number;
  selectCallback: () => void;
}

const AuthButton = ({ buttonTitle, buttonKey, selectCallback }: IAuthButtonProps) => {

  return (
    <div className="button-item" onClick={() => selectCallback} >
      <input
        type="radio"
        name="auth"
        id={`AuthInput${buttonKey}`}
      />
      <label htmlFor={`AuthInput${buttonKey}`}>
        {buttonTitle}
      </label>
    </div>
  );
};

export default AuthButton;
