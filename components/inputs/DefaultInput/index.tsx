import './index.scss';

interface IDefaultInputProps {
  inputTitle: string;
  inputType?: string;
  val: string;
  onChangeCallback: (e: string) => void;
  error?: boolean;
  defaultValue?: string;
  parallel?: boolean;
}

const DefaultInput = ({ inputTitle, inputType, val, onChangeCallback, error, defaultValue, parallel }: IDefaultInputProps) => {

  return (
    <>
      <label htmlFor="DefaultInput" className={`default-input-wrapper ${parallel && 'parallel'}`}>
        {inputTitle}
        <input
          autoComplete="off"
          type={inputType}
          value={val}
          id="DefaultInput"
          onChange={(e) => onChangeCallback(e.target.value)}
          className={defaultValue && 'default-value'} />
      </label>
      { inputTitle === '비밀번호 확인' && error && <p>입력하신 비밀번호가 일치하지 않아요.</p> }
    </>
  );
};

export default DefaultInput;
