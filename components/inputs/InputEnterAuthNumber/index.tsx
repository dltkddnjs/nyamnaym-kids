'use client';

import Image from 'next/image';
import { useState } from 'react';
import CheckedIcon from '../../../public/assets/icons/ic-checked-green-20x20.svg';
import './index.scss';

interface IInputEnterAuthNumberProps {
  inputTitle: string;
}

const InputEnterAuthNumber = ({ inputTitle }: IInputEnterAuthNumberProps) => {
  const [requestState] = useState<'before' | 'timeInit' | 'timeFinish' | 'success' | 'error'>('before');

  return (
    <div className="input-enter-auth-number-container">
      <div className="input-wrapper">
        <label htmlFor="EnterAuthNumberInput" className={`${requestState === 'before' && 'before'}`}>
          {inputTitle}
          {requestState === 'before' ?
            <input type="number" id="EnterAuthNumberInput" disabled />
            : <input type="number" id="EnterAuthNumberInput" className={requestState} />}
          {requestState === 'success' && 
          <Image src={CheckedIcon} alt="icon" width={20} height={20} className="check-icon" />}
          {requestState === 'timeInit' && 
          <p className="request-time">3:00</p>}
        </label>
        {requestState === 'success' && <p>성공적으로 인증됐어요</p>}
      </div>
    </div>
  );
};

export default InputEnterAuthNumber;
