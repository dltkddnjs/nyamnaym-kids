/* eslint-disable max-len */
import { useEffect, useState } from 'react';
import { useTimer } from '@/hooks/useTimer';
import './index.scss';

interface IInputRequestAuthNumberProps {
  inputTitle: string;
  inputType: string;
  val: string;
  phoneNum: string;
  onChangeCallback: (e: string) => void;
  guideText?: string;
}

const InputRequestAuthNumber = ({ inputTitle, inputType, val, phoneNum, onChangeCallback, guideText }: IInputRequestAuthNumberProps) => {

  /* 인증번호 요청시 제한시간 타이머 */
  const { count, timerCallback } = useTimer();

  const [reSend, setReSend] = useState(0);

  /* 재전송 버튼부턴 1분이 지났을 때부터 다시 클릭이 가능 */
  useEffect(() => {
    if (reSend > 0) {
      // 1분 후 다시 클릭할 수 있도록 하는 함수
    }
  }, [reSend]);

  return (
    <div className="input-request-auth-number-container">
      <div className="input-wrapper">
        <label htmlFor="RequestAuthNumberInput">
          {inputTitle}
          <input type={inputType} id="RequestAuthNumberInput" value={val} onChange= {(e) => onChangeCallback(e.target.value)} />
          {count}
          {/* 
          인증번호가 일치하면 타이머를 그만 보여주고 체크표시로 바꿔 줘야함
          그럼 useTimer가 인증완료 상태를 알아야겠지?
        */}
        </label>
        {guideText && <p>{guideText}</p>}
      </div>
      <button disabled={phoneNum && phoneNum.length === 11 ? false : true } type="button" onClick={() => { timerCallback(); setReSend(reSend + 1);}} >인증번호 {reSend > 1 && '재'}요청</button>
    </div>
  );
};

export default InputRequestAuthNumber;
