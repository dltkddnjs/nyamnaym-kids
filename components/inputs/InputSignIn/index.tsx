import Image from 'next/image';
import React from 'react';
import DeleteIcon from '../../../public/assets/icons/ic-circle-delete-20x20.svg';
import './index.scss';

interface IInputSignInProps {
  inputTitle: string;
  type: 'email' | 'password';
  val: string; 
  onChangeCallback:(id: string) => void;
  deleteCallback: () => void;
  error: boolean;
}

const InputSignIn = ({ inputTitle, type, val, onChangeCallback, deleteCallback, error }: IInputSignInProps) => {

  return (
    <div className={`input-wrapper ${error && 'error'}`}>
      <label htmlFor="signInput">
        {inputTitle}
        <input type={type} id="signInput" value={val} onChange={(e) => onChangeCallback(e.target.value)} />
        { val && <Image className="delete-icon" src={DeleteIcon} alt="icon" width={20} height={20} onClick={deleteCallback} /> }
      </label>
      { inputTitle === '비밀번호' && error === true && <p>아이디 또는 비밀번호를 확인 후 다시 시도해주세요.</p> }
    </div>
  );
};

export default InputSignIn;