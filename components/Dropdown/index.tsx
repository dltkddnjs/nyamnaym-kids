'use client';

import Image from 'next/image';
import { useState } from 'react';
import ArrowBottomIcon from '../../public/assets/icons/ic-bottom-arrow-gray-14x14.svg';
import './index.scss';

const Dropdown = () => {
  const [toggle, setToggle] = useState(false);
  const [selectedItem, setSelectedItem] = useState<string | number>('선택');

  const onClick = (optionName: string | number) => {
    setSelectedItem(optionName);
    setToggle(prev => !prev);
  };

  return (
    <div className="dropdown-container">
      <div
        className={`header-wrapper ${toggle && 'toggle'}`}
        onClick={() => setToggle(prev => !prev)}>
        {selectedItem}
        <Image src={ArrowBottomIcon} alt="icon" width={14} height={14} />
      </div>
      {toggle && 
      
        <div className="select-wrapper">
          { Array.from(Array(6), (_, i) => i + 1 ).map((data, index) =>
            ( <div className="select-item" key={index} onClick={() => onClick(data)}> {data} </div>) )}
        </div>}
      {toggle && 
        <div className="transparent-dim" onClick={() => setToggle(false)} />
      }
    </div>
  );
};

export default Dropdown;
