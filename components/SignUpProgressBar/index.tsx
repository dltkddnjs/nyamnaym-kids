import './index.scss';

interface ISignUpProgressBarProps {
  currentProgress: number;
}

const SignUpProgressBar = ({ currentProgress }: ISignUpProgressBarProps) => {
  return (
    <div className="sign-up-progress-bar-container">
      <div className={`progress-bar-item ${currentProgress === 1 && 'current'}`}>1</div>
      <div className={`progress-bar-item ${currentProgress === 2 && 'current'}`}>2</div>
      <div className={`progress-bar-item ${currentProgress === 3 && 'current'}`}>3</div>
      <div className={`progress-bar-item ${currentProgress === 4 && 'current'}`}>4</div>
      <div className={`progress-bar-item ${currentProgress === 5 && 'current'}`}>5</div>
      <div className="parallel-line" />
    </div>
  );
};

export default SignUpProgressBar;
