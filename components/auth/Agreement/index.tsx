import React from 'react';
import './index.scss';
import NextButton from '@/components/buttons/NextButton';
import CheckIcon from '../../../public/assets/icons/ic-circle-check-gray-24x24.svg';
import Image from 'next/image';

const Agreement = () => {
  return (
    <div className="agreement-container">
      <div className="agreement-wrapper">
        <div className="all-agree-wrapper">
          <Image src={CheckIcon} alt="icon" />
          전체동의
        </div>
        <div className="each-agree-wrapper">
          <div className="each-agree-item">
            <div className="header">
              <Image src={CheckIcon} alt="icon" />
              [필수] 개인정보처리방침 동의
            </div>
            <div className="body">
                text 1line <br/>
                text 2line <br/>
                text 3line <br/>
                text 4line <br/>
                text 5line <br/>
                text 6line <br/>
                text 7line <br/>
                text 8line <br/>
                text 9line
            </div>
          </div>
          <div className="each-agree-item">
            <div className="header">
              <Image src={CheckIcon} alt="icon" />
              [필수] 이용약관 동의
            </div>
            <div className="body">
                text 1line <br/>
                text 2line <br/>
                text 3line <br/>
                text 4line <br/>
                text 5line <br/>
                text 6line <br/>
                text 7line <br/>
                text 8line <br/>
                text 9line
            </div>
          </div>
          <div className="each-agree-item">
            <div className="header">
              <Image src={CheckIcon} alt="icon" />
              [선택] 마케팅 이용 동의
            </div>
            <div className="body">
                text 1line <br/>
                text 2line <br/>
                text 3line <br/>
                text 4line <br/>
                text 5line <br/>
                text 6line <br/>
                text 7line <br/>
                text 8line <br/>
                text 9line
            </div>
          </div>
        </div>
      </div>
      <NextButton disabled />
    </div>
  );
};

export default Agreement;
