import DefaultInput from '@/components/inputs/DefaultInput';
import NextButton from '@/components/buttons/NextButton';
import './index.scss';

const InstitutionCode = () => {
  return (
    <div className="institution-code-wrapper">
      <DefaultInput inputTitle="기관코드" inputType="number" />
      <NextButton disabled />
      <div className="bottom-wrapper">
        <p>혹시 원장님이 아니신가요?</p>
        <p>선생님이라면 이메일로 전달된 링크를 통해 가입을 진행해주세요.</p>
      </div>
    </div>
  );
};

export default InstitutionCode;
