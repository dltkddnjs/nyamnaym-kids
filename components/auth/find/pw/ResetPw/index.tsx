'use client';

import React, { useEffect, useState } from 'react';

import DefaultInput from '@/components/inputs/DefaultInput';
import NextButton from '@/components/buttons/NextButton';

interface IResetPwProps {
  stepCallback: () => void;
}

const ResetPw = ({ stepCallback }: IResetPwProps) => {

  const [pw, setPw] = useState({ pw1: '', pw2: '' });
  const [error, setError] = useState(false);

  const onChangePw1Callback = (e: string) => {
    setPw((prev) => ({ ...prev, pw1: e }));
  };

  const onChangePw2Callback = (e: string) => {
    setPw((prev) => ({ ...prev, pw2: e }));
  };

  useEffect(() => {
    if (pw.pw1 !== pw.pw2) {
      setError(true);
    } else {
      setError(false);
    }
  }, [pw]);
  
  return (
    <React.Fragment>
      <DefaultInput inputTitle="비밀번호" inputType="password" val={pw.pw1} onChangeCallback={onChangePw1Callback} />
      <DefaultInput inputTitle="비밀번호 확인" inputType="password" val={pw.pw2} onChangeCallback={onChangePw2Callback} error={error} />
      <NextButton disabled={ pw.pw1.length > 7 && pw.pw2.length > 7 && pw.pw1 === pw.pw2 ? false : true } stepCallback={stepCallback}/>
      {/* 상호 비밀번호 일치여부는 필드 입력동안에 오류출력 확정 */}
      {/* 그렇다면 정규식 체크 오류출력은 어디서 이루어 져야함?... */}

      {/* 비번1 비번2 상호 일치 시 버튼활성화 */}
    </React.Fragment>
  );
};

export default ResetPw;