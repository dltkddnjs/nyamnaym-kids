'use client';
import React, { useState } from 'react';

import Link from 'next/link';
import Image from 'next/image';

import DefaultInput from '@/components/inputs/DefaultInput';
import InputRequestAuthNumber from '@/components/inputs/InputRequestAuthNumber';
import NextButton from '@/components/buttons/NextButton';
import RightArrowIcon from '@/public/assets/icons/ic-right-arrow-black-16x16.svg';
import { checkNumberRegex } from '@/utils/check';

interface IFindPwInitProps {
  stepCallback: () => void;
}

const FindPwInit = ({ stepCallback }: IFindPwInitProps ) => {

  const [info, setInfo] = useState({ id: '', name: '', phone: '', code: '' });

  const onChangeIdCallback = (e: string) => {
    setInfo((prev) => ({ ...prev, id: e }));
  };

  const onChangeNameCallback = (e: string) => {
    setInfo((prev) => ({ ...prev, name: e }));
  };

  const onChangePhoneCallback = (e: string) => {
    if (e.length > 11) return;
    setInfo((prev) => ({ ...prev, phone: e.replace(checkNumberRegex, '') }));
  };

  const onChangeCodeCallback = (e: string) => {
    if (e.length > 6) return;
    setInfo((prev) => ({ ...prev, code: e.replace(checkNumberRegex, '') }));
  };

  return (
    <React.Fragment>
      <DefaultInput inputTitle="아이디(이메일)" inputType="email" val={info.id} onChangeCallback={onChangeIdCallback} />
      <DefaultInput inputTitle="이름" inputType="text" val={info.name} onChangeCallback={onChangeNameCallback} />
      <DefaultInput inputTitle="휴대전화" inputType="text" val={info.phone} onChangeCallback={onChangePhoneCallback} />
      <InputRequestAuthNumber inputTitle="인증번호" inputType="text" val={info.code} phoneNum={info.phone} onChangeCallback={onChangeCodeCallback} />
      <NextButton disabled={ info.code.length === 6 ? false : true} stepCallback={stepCallback} /> 
      {/* 모든 사항을 입력하고, 인증이 완료된 상태여야 다음으로 넘어갈 수 있음 */}
      {/* 다음버튼을 눌렀는데, 아이디가 유효하지 않다면 '입력한 아이디가 존재하지 않습니다' 출력 */}
      <div className="bottom-wrapper">
        <p>아이디가 기억나지 않으시나요?</p>
        <Link href="find-id">아이디 찾기<Image src={RightArrowIcon} alt="icon" width={16} height={16} /></Link>
      </div>
    </React.Fragment>
  );
};

export default FindPwInit;