import Link from 'next/link';

const CompleteResetPw = () => {
  return (
    <div className="complete-reset-pw-container">
      <Link href="/">로그인 페이지로 이동</Link>
    </div>
  );
};

export default CompleteResetPw;