'use client';
import React, { useState } from 'react';
import './index.scss';

import AuthButton from '@/components/buttons/AuthButton';
import NextButton from '@/components/buttons/NextButton';

interface IFindIdInitProps {
  stepCallback: () => void
}

const FindIdInit = ({ stepCallback }:IFindIdInitProps ) => {

  const [select, setSelect] = useState(0);

  const selectPhone = () => {
    setSelect(1);
  };

  return (
    <React.Fragment>
      <div className="auth-button-wrapper">
        <AuthButton buttonTitle="휴대전화 인증" buttonKey={1} selectCallback={selectPhone} />
        {/* <AuthButton buttonTitle="이메일 인증" buttonKey={2} selectCallback={selectEmail} /> */}
      </div>
      <NextButton disabled={select === 0 ? true : false} stepCallback={stepCallback} />
    </React.Fragment>
  );
};

export default FindIdInit;