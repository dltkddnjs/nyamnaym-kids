import Link from 'next/link';
import './index.scss';

const CompleteFindId = () => {
  return (
    <div className="complete-find-id-container">
      <p>nuvi1234@gmail.com</p>
      <div className="button-wrapper">
        <Link href="/auth/find-pw"><button>비밀번호 찾기</button></Link>
        <Link href="/"><button>로그인</button></Link>
      </div>
    </div>
  );
};

export default CompleteFindId;