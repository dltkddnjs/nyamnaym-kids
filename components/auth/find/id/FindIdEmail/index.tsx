import React from 'react';

import DefaultInput from '@/components/inputs/DefaultInput';
import InputRequestAuthNumber from '@/components/inputs/InputRequestAuthNumber';
import NextButton from '@/components/buttons/NextButton';

const FindIdEmail = () => {

  return (
    <React.Fragment>
      <DefaultInput inputTitle="이름" inputType="text" />
      <InputRequestAuthNumber inputTitle="이메일" inputType="email" />
      <NextButton disabled={true} />
    </React.Fragment>
  );
};

export default FindIdEmail; 