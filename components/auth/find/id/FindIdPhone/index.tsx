import React, { useState } from 'react';

import DefaultInput from '@/components/inputs/DefaultInput';
import InputRequestAuthNumber from '@/components/inputs/InputRequestAuthNumber';
import NextButton from '@/components/buttons/NextButton';
import { checkNumberRegex } from '@/utils/check';

interface IFindIdPhoneProps {
  stepCallback: () => void;
}

const FindIdPhone = ({ stepCallback }:IFindIdPhoneProps ) => {

  const [info, setInfo] = useState({ name: '', phone: '', code: '' });

  const onChangeNameCallback = (e: string) => {
    setInfo((prev) => ({ ...prev, name: e }));
  };

  const onChangePhoneCallback = (e: string) => {
    if (e.length > 11) return;
    setInfo((prev) => ({ ...prev, phone: e.replace(checkNumberRegex,'') }));
  };

  const onChangeCodeCallback = (e: string) => {
    if (e.length > 6) return;
    setInfo((prev) => ({ ...prev, code: e.replace(checkNumberRegex, '') }));
  };

  return (
    <React.Fragment>
      <DefaultInput inputTitle="이름" inputType="text" val={info.name} onChangeCallback={onChangeNameCallback} />
      <DefaultInput inputTitle="휴대전화" inputType="text" val={info.phone} onChangeCallback={onChangePhoneCallback} />
      <InputRequestAuthNumber inputTitle="인증번호" inputType="text" val={info.code} phoneNum={info.phone} onChangeCallback={onChangeCodeCallback} />
      <NextButton disabled={false} stepCallback={stepCallback} />
      {/* 버튼 클릭했는데, 가입 시 등록한 이름이나 휴대전화번호가 아닐 경우 에러메세지 출력/ 맞으면 stepCallback 진행 */}
    </React.Fragment>
  );
};

export default FindIdPhone; 