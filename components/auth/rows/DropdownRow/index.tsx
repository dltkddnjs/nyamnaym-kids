import Dropdown from '@/components/Dropdown';
import './index.scss';

interface IDropdownRowProps {
  rowTitle: string;
}

const DropdownRow = ({ rowTitle }: IDropdownRowProps) => {
  return (
    <div className="dropdown-row-container">
      <p>{rowTitle}</p>
      <Dropdown />
    </div>
  );
};

export default DropdownRow;
