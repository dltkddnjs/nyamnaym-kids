import SignUpProgressBar from '@/components/SignUpProgressBar';
import './index.scss';
import NextButton from '@/components/buttons/NextButton';
import InputRequestAuthNumber from '@/components/inputs/InputRequestAuthNumber';
import InputEnterAuthNumber from '@/components/inputs/InputEnterAuthNumber';

const Process2 = () => {
  return (
    <div className="process2-container">
      <SignUpProgressBar currentProgress={2} />
      <div className="inputs-wrapper">
        <InputRequestAuthNumber
          inputTitle="휴대전화"
          inputType="number"
          guideText="해당 전화번호는 비밀번호 분실 시 신원을 확인하고 비밀번호를 재설정하는데 사용돼요. 또한, 서비스 관련 공지 및 안내 시에도 사용되니 항상 사용할 수 있는 전화번호를 입력해주세요." />
        <InputEnterAuthNumber inputTitle="휴대전화 인증번호" />
      </div>
      <NextButton disabled/>
    </div>
  );
};

export default Process2;
