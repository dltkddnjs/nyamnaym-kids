import SignUpProgressBar from '@/components/SignUpProgressBar';
import DefaultInput from '@/components/inputs/DefaultInput';
import InputRequestAuthNumber from '@/components/inputs/InputRequestAuthNumber';
import NextButton from '@/components/buttons/NextButton';
import InputEnterAuthNumber from '@/components/inputs/InputEnterAuthNumber';
import './index.scss';

const Process1 = () => {
  return (
    <div className="process1-container">
      <SignUpProgressBar currentProgress={1} />
      <div className="inputs-wrapper">
        <DefaultInput inputTitle="이름" defaultValue="김누비" />
        <InputRequestAuthNumber 
          inputTitle="이메일"
          inputType="email"
          guideText="입력하신 이메일 주소는 신규 누비랩 계정 아이디로 사용돼요" />
        <InputEnterAuthNumber inputTitle="이메일 인증번호" />
        <DefaultInput inputTitle="비밀번호" inputType="password" />
        <DefaultInput inputTitle="비밀번호 확인" inputType="password" />
      </div>
      <NextButton disabled />
    </div>
  );
};

export default Process1;
