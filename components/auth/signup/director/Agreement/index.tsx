'use client';
import './index.scss';

import { useAgreement } from '@/contexts/AgreementProvider';

const Agreement = () => {
  
  const { 
    agree,
    agreeEntireTerms,
    togglePrivacyPolicy,
    toggleTermsAndCondition,
    toggleMarketingUsage,
    inspectCondition
  } = useAgreement();
  
  console.log(agree);

  return (
    <div>
      약관 동의

      <div onClick={agreeEntireTerms}>전체동의</div> {/* agree state 중 하나라도 false면 체크란 해제 */}
      <div onClick={togglePrivacyPolicy}>[필수] 개인정보처리방침 동의</div>
      <div onClick={toggleTermsAndCondition}>[필수] 이용약관 동의</div>
      <div onClick={toggleMarketingUsage}>[선택] 마케팅 이용 동의</div>
      <button onClick={inspectCondition}>다음</button> {/* agree.privacyPolicy && agree.marketingUsage 트루면 검정색에 cursor: pointer 효과 */}
    </div>
  );
};

export default Agreement;