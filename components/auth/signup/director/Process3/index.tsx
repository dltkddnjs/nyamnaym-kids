import SignUpProgressBar from '@/components/SignUpProgressBar';
import './index.scss';
import NextButton from '@/components/buttons/NextButton';
import DefaultInput from '@/components/inputs/DefaultInput';
import DropdownRow from '@/components/auth/rows/DropdownRow';

const Process3 = () => {
  return (
    <div className="process3-container">
      <SignUpProgressBar currentProgress={3} />
      <div className="inputs-wrapper">
        <DefaultInput inputTitle="원 이름" defaultValue="누비어린이집" parallel />
        <DefaultInput inputTitle="원장님 이름" defaultValue="김누비" parallel />
        <DefaultInput inputTitle="원 전화번호" defaultValue="02-1234-5678" parallel />
        <DefaultInput inputTitle="원 위치" defaultValue="서울시 강남구 삼성동 ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ" parallel />
        <div className="detail-info-wrapper">
          <p>원 상세정보</p>
          <div className="dropdown-row-wrapper">
            <DropdownRow rowTitle="설립유형" />
            <DropdownRow rowTitle="급식운영방식" />
            <DropdownRow rowTitle="영양(교)사 배치여부" />
            <DropdownRow rowTitle="어린이급식관리지원센터 등록/관리" />
            <DropdownRow rowTitle="NEIS 급식관리 시스템 활용" />
            <DropdownRow rowTitle="배식장소" />
            <DropdownRow rowTitle="점심시간" />
          </div>
        </div>
      </div>
      <NextButton disabled/>
    </div>
  );
};

export default Process3;
