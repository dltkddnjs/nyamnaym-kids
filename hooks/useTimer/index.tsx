/* eslint-disable max-len */
import { useEffect, useState } from 'react';

export const useTimer = () => {

  const [operate, setOperate] = useState(false);
  
  const [timeValue, setTimeValue] = useState(0);
  const [count, setCount] = useState('0:00');

  const timerCallback = () => {
    setTimeValue(3000); /* 180000 */
    setOperate(true);
  };

  /* 설정시간 */
  const settingTime = (val: number) => {
    setCount(`${new Date(Number(val)).getMinutes()}:${new Date(Number(val)).getSeconds() < 10 ? 0 : ''}${new Date(Number(val)).getSeconds()}`);
  };

  useEffect(() => {
    if (!operate) return;

    const timer = setInterval(() => { 
      setTimeValue(prev => prev - 1000);
    }, 1000);

    settingTime(timeValue);

    if (timeValue <= 0) { 
      setOperate(false);
      clearInterval(timer);
    }
    
    return () => clearInterval(timer);
  }, [operate, timeValue]);
  
  return {
    count,
    timerCallback
  };
};