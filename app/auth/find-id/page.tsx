'use client';
import { useState } from 'react';
import './page.scss';

import FindIdInit from '@/components/auth/find/id/FindIdInit';
import FindIdPhone from '@/components/auth/find/id/FindIdPhone';
import CompleteFindId from '@/components/auth/find/id/CompleteFindId';

const FindId = () => {

  const [step, setStep] = useState(1); /* setStep */

  const step2Callback = () => {
    setStep(2);
  };

  const step3Callback = () => {
    setStep(3);
  };

  return (
    <div className="find-id-container">
      <p>{ step !== 3 ? '아이디 찾기에 필요한 정보를 입력해주세요' : '아이디를 확인해주세요' } </p>
      { step === 1 && <FindIdInit stepCallback={step2Callback} /> }
      { step === 2 && <FindIdPhone stepCallback={step3Callback} /> }
      { step === 3 && <CompleteFindId /> }
    </div>
  );
};

export default FindId;