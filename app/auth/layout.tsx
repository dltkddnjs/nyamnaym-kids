'use client';

import { useState, useEffect } from 'react';
import './layout.scss';

import Image from 'next/image';
import Logo from '../../public/assets/icons/logo/logo-nuvilab-black-250x59.svg';

const AuthLayout = ({ children }: { children: React.ReactNode }) => {
  const [mounted, setMounted] = useState<boolean>(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  return (mounted && 
      <div className="auth-layout">
        <main>
          <Image src={Logo} alt="logo" width={250} height={59} />
          <section>{children}</section>
        </main>
      </div>
  );
}; 

export default AuthLayout;