const SignUpTeacher = () => {
  return (
    <div>
      1. 교사 가입 시작 (기관명&반 확인) SignUpInit인컴포넌트
      2. 교사 서비스약관 동의 Agreement 컴포넌트
      3. 교사 비밀번호 설정 Process1 컴포넌트
      4. 교사 휴대전화인증 Process2 컴포넌트
    </div>
  );
};

export default SignUpTeacher;