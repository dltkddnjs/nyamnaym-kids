'use client';

// import { useState } from 'react';

const SignUpDirector = () => {
  
  /* 입력한 코드 값이 존재하고 && 1글자 이상일 시 다음버튼 enable */
  // const [code, setCode] = useState('');
  // const inputInstitutionCode = (code: string) => {
  //   if (code === '' || code.length !== 6) return;
    
  //   setCode(code);
  // };

  return (
    <div>
      1. 원장님 가입시작 SignUpInit 컴포넌트
      2. 서비스약관 동의 Agreement 컴포넌트
      3. 정보입력 Process1 
      4. 휴대전화인증 Process2
      5. 원장상세정보 Process3
      6. 반정보등록 Process4
      7. 교사초대 Process5
    </div>
  );
};

export default SignUpDirector;