const DashboardLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      <aside>left sidebar 영역(사이드바 컴포넌트로 만들어주자)</aside>
      {children}
    </div>
  );
};

export default DashboardLayout;