/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable max-len */
'use client'; 

import { FormEvent, useEffect, useState } from 'react';
import './page.scss';

import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/navigation';

import password from '../utils/password/index';

import InputSignIn from '@/components/inputs/InputSignIn';
import Logo from '@/public/assets/icons/logo/logo-nuvilab-black-122x32.svg';

const Home = () => {

  const router = useRouter();
  const [account, setAccount] = useState({ id: '', pw: '' });
  const [isActive, setIsActive] = useState(false);

  const [error, setError] = useState(false);

  const onChangeIdCallback = ( id:string ) => {
    setAccount((prev) => ({ ...prev, id: id }));
    setError(false);
  };

  const onChangePwCallback = ( password: string ) => {
    setAccount((prev) => ({ ...prev, pw: password }));
    setError(false);
  };

  const deleteIdCallback = () => {
    setAccount((prev) => ({ ...prev, id: '' }));
  };

  const deletePwCallback = () => {
    setAccount((prev) => ({ ...prev, pw: '' }));
  };

  /* 로그인요청 */ /* <- 입력한 값을 hash한다음에 그걸 여기에 복붙해서 대조 실험해보면 되는 것???? */
  const requestLogin = (e:FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (account.id !== 'nuvi1234@gmail.com' || account.pw !== '123qwe!') {
      return setError(true);
    } else {
      router.push('/dashboard/home');
    }
    // getInstance().post<ILoginRes>('/auth/login', { email: account.id || '', password: transHash(String(pw)) || '' }).then(val => {
    //   /* 1. success면 main으로 이동 */
    //   if (val.data.success) {
    //     setLoginState(val.data.payload);
    //     localStorage.setItem('accessToken', val.data.payload.accessToken);
    //     localStorage.setItem('refreshToken', val.data.payload.refreshToken);
    //     router.push('/dashboard/home');
    //   }
    //   /* 2. 실패면 */
    //   else { console.log('로그인 실패'); }
    // });
  };

  useEffect(() => {
    if (account.id.includes('@') && account.pw.length > 7 ) { /* 정규식 정해지면 수정할 것! */
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [account]);

  return (
    <div className="home-container">

      <header >
        <div className="header-wrapper">
          <Link href="/"><Image src={Logo} alt="logo" width={122} height={32} /></Link>
          <div className="menu-wrapper">
            <Link href="https://nuvilab-kids.oopy.io/guide/quickguide" className="menu-item"><p>서비스 소개</p></Link>
            <Link href="" className="menu-item"><p>가격 정책</p></Link>
            <Link href="https://nuvilab-kids.oopy.io/faq/director" className="menu-item"><p>FAQ</p></Link>
          </div>
        </div>
      </header>

      <main>
        <article>섹션</article> 
        <aside>
          <form onSubmit={(e) => requestLogin(e)}>
            <InputSignIn inputTitle="아이디(이메일)" type="email" val={account.id} onChangeCallback={onChangeIdCallback} deleteCallback={deleteIdCallback} error={error} />
            <InputSignIn inputTitle="비밀번호" type="password" val={account.pw} onChangeCallback={onChangePwCallback} deleteCallback={deletePwCallback} error={error} />
            <button disabled={isActive && !error ? false : true} type="submit">로그인</button>
            <div className="bottom-box-wrapper">
              <Link href="auth/find-id" className="bottom-box-item">아이디 찾기</Link>
              <Link href="auth/find-pw" className="bottom-box-item">비밀번호 찾기</Link>
              <Link href="auth/director-signup" className="bottom-box-item">기관 회원 가입</Link>
            </div>
          </form>
        </aside>
      </main>
    </div>
  );
};

export default Home;
