'use client';

import { PropsWithChildren, createContext, useContext, useState } from 'react';
import { useRouter } from 'next/navigation';

interface IAgreementContextProps {  
  agree: {
    privacyPolicy: boolean;
    termsAndCondition: boolean;
    marketingUsage: boolean;
  };
  agreeEntireTerms: () => void;
  togglePrivacyPolicy: () => void;
  toggleTermsAndCondition: () => void;
  toggleMarketingUsage: () => void;
  inspectCondition: () => void
}

const AgreementContext = createContext<IAgreementContextProps>({

} as IAgreementContextProps );

const AgreementProvider = ({ children }: PropsWithChildren) => {
  
  const router = useRouter();
  const [agree, setAgree] = useState({
    privacyPolicy: false,
    termsAndCondition: false,
    marketingUsage: false
  });
  
  const agreeEntireTerms = () => {
    setAgree((prev) => ({ ...prev, privacyPolicy: true, termsAndCondition: true, marketingUsage: true }));
  };

  const togglePrivacyPolicy = () => {
    if (agree.privacyPolicy) {
      setAgree((prev) => ({ ...prev, privacyPolicy: false }));
    } else {
      setAgree((prev) => ({ ...prev, privacyPolicy: true }));
    }
  };

  const toggleTermsAndCondition = () => {
    if (agree.termsAndCondition) {
      setAgree((prev) => ({ ...prev, termsAndCondition: false }));
    } else {
      setAgree((prev) => ({ ...prev, termsAndCondition: true }));
    }
  };

  const toggleMarketingUsage = () => {
    if (agree.marketingUsage) {
      setAgree((prev) => ({ ...prev, marketingUsage: false }));
    } else {
      setAgree((prev) => ({ ...prev, marketingUsage: true }));
    }
  };

  const inspectCondition = () => {
    if (!agree.privacyPolicy || !agree.termsAndCondition) console.log('필수 동의란에 체크 해주세요.');
    else {
      /* 1. 원장님 자격으로 가입시 "원장가입절차"로 */ 
      router.push('/auth/director-signup-process'); 

      /* 2. 교사 자격으로 가입시 "교사가입절차"로 */
    }
  };

  return (
    <AgreementContext.Provider value={{ 
      agree,
      agreeEntireTerms,
      togglePrivacyPolicy,
      toggleTermsAndCondition,
      toggleMarketingUsage,
      inspectCondition
    }}>
      {children}
    </AgreementContext.Provider>
  );
};

export default AgreementProvider;
export const useAgreement = () => useContext(AgreementContext);