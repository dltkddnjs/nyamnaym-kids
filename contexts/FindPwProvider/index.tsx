import { PropsWithChildren, createContext, useContext } from 'react';

interface IFindPwContextProps {
  
}

const FindPwContext = createContext({
  
} as IFindPwContextProps);

const FindPwProvider = ({ children }:PropsWithChildren) => {

  return (
    <FindPwContext.Provider value={{ }}>
      {children}
    </FindPwContext.Provider>
  );
};
export default FindPwProvider;
export const useFindPw = () => useContext(FindPwContext);
